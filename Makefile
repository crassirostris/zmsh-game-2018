DOCKERFILE_PATH = ./image/Dockerfile
REGISTRY = registry.gitlab.com
IMAGE_NAME = crassirostris/zmsh-game-2018
TAG = 0.8.1

build:
	docker build -f "$(DOCKERFILE_PATH)" -t "$(REGISTRY)/$(IMAGE_NAME):$(TAG)" .

push:
	docker push "$(REGISTRY)/$(IMAGE_NAME):$(TAG)"
