#!/usr/bin/env python

from enum import Enum
from enum import auto

from mongoengine import Document
from mongoengine.fields import BooleanField
from mongoengine.fields import LongField
from mongoengine.fields import ListField
from mongoengine.fields import StringField
from mongoengine.fields import ObjectIdField
from mongoengine.fields import ComplexDateTimeField

class GameStage(Enum):
    PRE_REGISTRATION = auto()
    REGISTRATION = auto()
    REGISTRATION_CLOSED = auto()
    SUBMITTING = auto()


class GameState(Document):
    current_stage = StringField(default=GameStage.PRE_REGISTRATION.name)


class Task(Document):
    file_url = StringField(required=True)


class Team(Document):
    name = StringField(required=True)
    task_id = ObjectIdField()


class Participant(Document):
    name = StringField(required=True)
    tg_id = LongField()
    tg_chat_id = LongField()
    team_id = ObjectIdField()


class Submission(Document):
    team_id = ObjectIdField(required=True)
    file_id = StringField(required=True)
    file_type = StringField(required=True)
    submit_time = ComplexDateTimeField(required=True)
