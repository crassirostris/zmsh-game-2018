#!/usr/bin/env python

from zmshgame.l10n.getmessage import get_message

__all__ = ('get_message')
