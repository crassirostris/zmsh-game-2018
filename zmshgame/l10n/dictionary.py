#!/usr/bin/env python

MESSAGES = {
# Admin updater.
    'ADMIN_UPDATER_HELP':
'''
Доступные команды:

/tasks - операции с тасками
/teams - операции с командами
/game_state - операции с состоянием игры
''',

# Admin updater game state.
    'ADMIN_UPDATER_GAME_STATE_HELP':
'''
Текущее состояние: {game_stage}

/open_registration
/start_game
/stop_game
/reset_game

/cancel - вернуться
''',

    'ADMIN_UPDATER_GAME_STATE_INVALID':
'''
Невалидное состояние: {stage}
''',

    'ADMIN_UPDATER_GAME_STATE_NOT_ENOUGH_TASKS':
'''
Недостаточно заданий!

В базе присутсвует {team_count} команд и {task_count} заданий.
''',

# Admin updater tasks.
    'ADMIN_UPDATER_TASKS_HELP':
'''
Что можно делать с тасками:

/add - добавить новый таск
/list - вывести список тасков
/get <id> - вывести таск с заданным id
/getall - показать все таски
/remove <id> - удалить таск с заданным id
/removeall - удалить все таски

/cancel - вернуться
''',

# Admin updater tasks add.
    'ADMIN_UPDATER_TASKS_ADD_HELP':
'''
В следующем сообщении пошлите ссылку на таск.

Введи /cancel чтобы вернуться.
''',

    'ADMIN_UPDATER_TASKS_ADD_CONTENT_NOT_FILE':
'''
Таск должен быть файлом.
''',

    'ADMIN_UPDATER_TASKS_ADD_UNKNOWN_TYPE':
'''
Неизвестный формат таска.
''',

    'ADMIN_UPDATER_TASKS_ADD_SUCCESS':
'''
Таск успешно добавлен!
''',

# Admin updater tasks list.
    'ADMIN_UPDATER_TASKS_LIST':
'''
Доступные таски:

{ids}
''',

# Admin updater tasks remove all.
    'ADMIN_UPDATER_TASKS_REMOVE_ALL_HELP':
'''
Точно удалить все таски?

/confirm - подтвердить
''',

# Admin updater teams.
    'ADMIN_UPDATER_TEAMS_HELP':
'''
Что можно делать с командами:

/add - добавить новую команду
/get <id> - вывести команду с заданным id
/getall - показать все команды
/remove <id> - удалить команду с заданным id
/removeall - удалить все команды

/cancel - вернуться
''',

    'ADMIN_UPDATER_TEAMS_ADD_HELP':
'''
В следующем сообщении пошли название команды.

Введи /cancel чтобы вернуться.
''',

    'ADMIN_UPDATER_TEAMS_ADD_INVALID_NAME':
'''
Некорректное имя, должно быть непустое и не только из вайтспейсов.
''',

    'ADMIN_UPDATER_TEAMS_ADD_SUCCESS':
'''
Команда успешно добавлена.
''',

    'ADMIN_UPDATER_TEAMS_ADD_UNAVAILABLE':
'''
Регистрация открыта или игра уже началась, добавлять команды уже нельзя.
''',

    'ADMIN_UPDATER_TEAMS_REMOVE_ALL_HELP':
'''
Точно удалить все команды?

/confirm - подтвердить
''',

# Main updater.
    'MAIN_UPDATER_HELP':
'''
Привет! Давай сыграем в игру. Жми /start
''',

    'MAIN_UPDATER_HELP_PRE_REGISTRATION':
'''
Регистрация еще не началась, приходи попозже.
''',

    'MAIN_UPDATER_HELP_REGISTRATION':
'''
Чтобы зарегестрироваться, жми /register
''',

    'MAIN_UPDATER_HELP_POST_REGISTRATION':
'''
Регистрация уже закрыта, better luck next time!
''',

    'MAIN_UPDATER_REGISTRATION_UNAVAILABLE':
'''
Регистрация сейчас недоступна.
''',

    'MAIN_UPDATER_ALREADY_REGISTERED':
'''
Привет, {name}! Твоя команда -- "{team_name}".

Игра скоро начнется
''',

    'MAIN_UPDATER_SUCCESSFULLY_REGISTERED':
'''
Ты успешно зарегестрирована!

Твоя команда -- "{team_name}".
''',

    'MAIN_UPDATER_HELP_SUBMITTNG':
'''
Привет, {name}! Твоя команда -- "{team_name}"

Чтобы послать ответ, жми /submit
''',

    'MAIN_UPDATER_SUBMITTING_HELP':
'''
Отправь видео следующим сообщением.
''',

    'MAIN_UPDATER_SUBMITTING_UNAVAILABLE':
'''
Посылка ответов сейчас недоступна.
''',

    'MAIN_UPDATER_SUBMISSION_INVALID':
'''
Какая-то проблема с файлом, попробуй еще раз.
''',

    'MAIN_UPDATER_SUBMISSION_SUCCESSFUL':
'''
Сабмит успешно сделан!
''',

    'MAIN_UPDATER_HELP_GAME_STARTED':
'''
Игра началась! Скорее жми /start
''',

# Generic.
    'GENERIC_UNKNOWN_ERROR':
'''
Что-то пошло не так.
''',

    'GENERIC_NO_ID':
'''
Не хватает параметра - id объекта.
''',

    'GENERIC_NOT_FOUND':
'''
Не найдено объекта по id.
''',
}
