#!/usr/bin/env python

from zmshgame.l10n.dictionary import MESSAGES

class LocalizationException(Exception):
    pass


def get_message(message_name):
    if message_name in MESSAGES:
        return MESSAGES[message_name]
    raise LocalizationException('Message "%s" not found' % message_name)
