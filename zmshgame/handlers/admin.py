#!/usr/bin/env python

from enum import Enum
from enum import auto
import logging
import random

from telegram.ext import Filters
from telegram.ext import Handler
from telegram.ext import MessageHandler
from telegram.ext import ConversationHandler
from telegram.ext import CommandHandler

from zmshgame.handlers.constants import GIF_MIME_TYPE
from zmshgame.handlers.base import BaseUpdater
from zmshgame.handlers.utils import get_current_game_stage
from zmshgame.handlers.utils import set_current_game_stage
from zmshgame.handlers.utils import task_to_document
from zmshgame.handlers.utils import OperationOnObject
from zmshgame.handlers.utils import OperationOnCollection
from zmshgame.l10n import get_message as _
from zmshgame.models import GameStage
from zmshgame.models import GameState
from zmshgame.models import Task
from zmshgame.models import Team
from zmshgame.models import Participant


class AdminUpdater(BaseUpdater):
    def __init__(self, main_updater, admins, *args):
        super(AdminUpdater, self).__init__(*args)
        self._register_handlers(main_updater, admins)

    def _register_handlers(self, main_updater, admins):
        dp = self.dispatcher
        dp.add_handler(AdminFilterHandler(admins))
        dp.add_handler(TasksHandler(self._print_help))
        dp.add_handler(TeamsHandler(self._print_help))
        dp.add_handler(GameStateHandler(self._print_help, main_updater))
        dp.add_handler(MessageHandler(Filters.all,
            self._print_help))

    def _print_help(self, bot, update):
        update.message.reply_text(_('ADMIN_UPDATER_HELP'))


class AdminFilterHandler(Handler):
    def __init__(self, admins, *args):
        super(AdminFilterHandler, self).__init__(lambda *_: None, *args)
        self.admins = admins

    def check_update(self, update):
        user = update.effective_user
        if user is None:
            return True

        allowed = self._is_admin(user)
        logging.info('User %s (%s %s) is running command "%s". Allowed: %s' % \
            (user.username, user.first_name, user.last_name,
            update.message.text, allowed))
        return not allowed

    def handle_update(self, update, dispatcher):
        pass

    def _is_admin(self, user):
        return not user is None and user.username in self.admins


class TasksHandlerState(Enum):
    INITIAL = auto()
    ADDING_TASK = auto()
    REMOVING_ALL_TASKS = auto()


class TasksHandler(ConversationHandler):
    def __init__(self, base_help):
        super(TasksHandler, self).__init__(
            entry_points=[CommandHandler('tasks', self._start)],
            states={
                TasksHandlerState.INITIAL: [
                    CommandHandler('add', self._start_add_task),
                    CommandHandler('list', OperationOnCollection(Task,
                        self._start, self._list_tasks).execute),
                    CommandHandler('get', OperationOnObject(Task,
                        self._start, self._get_task).execute, pass_args=True),
                    CommandHandler('getall', OperationOnCollection(Task,
                        self._start, self._get_all_tasks).execute),
                    CommandHandler('remove', OperationOnObject(Task,
                        self._start, self._remove_task).execute,
                        pass_args=True),
                    CommandHandler('removeall', self._start_remove_all_tasks),
                    CommandHandler('cancel', self._cancel),
                    MessageHandler(Filters.all, self._start),
                ],
                TasksHandlerState.ADDING_TASK: [
                    CommandHandler('cancel', self._start),
                    MessageHandler(Filters.all, self._add_task),
                ],
                TasksHandlerState.REMOVING_ALL_TASKS: [
                    CommandHandler('confirm', self._remove_all_tasks),
                    MessageHandler(Filters.all, self._start),
                ],
            },
            fallbacks=[MessageHandler(Filters.all, self._fallback)]
        )
        self.base_help = base_help

    def _start(self, bot, update):
        update.message.reply_text(_('ADMIN_UPDATER_TASKS_HELP'))
        return TasksHandlerState.INITIAL

    def _cancel(self, bot, update):
        self.base_help(bot, update)
        return ConversationHandler.END

    def _fallback(self, bot, update):
        update.message.reply_text(_('GENERIC_UNKNOWN_ERROR'))
        logging.warn('Unknown command in the current state: %s' %
            update.message.text)
        return ConversationHandler.END

    def _start_add_task(self, bot, update):
        update.message.reply_text(_('ADMIN_UPDATER_TASKS_ADD_HELP'))
        return TasksHandlerState.ADDING_TASK

    def _add_task(self, bot, update):
        try:
            Task(file_url=update.message.text).save()
            update.message.reply_text(_('ADMIN_UPDATER_TASKS_ADD_SUCCESS'))

            return self._start(bot, update)
        except Exception as e:
            update.message.reply_text(_('GENERIC_UNKNOWN_ERROR'))
            logging.warn(e)
        return self._start_add_task(bot, update)

    def _list_tasks(self, bot, update, tasks):
        ids = [ str(task.id) for task in Task.objects ]
        update.message.reply_text(_('ADMIN_UPDATER_TASKS_LIST').format(
            ids='\n'.join(ids)))

    def _get_task(self, bot, update, task):
        update.message.reply_video(task_to_document(task))

    def _get_all_tasks(self, bot, update, tasks):
        for task in tasks:
            try:
                update.message.reply_text(str(task.id))
                update.message.reply_video(task_to_document(task))
            except Exception as e:
                update.message.reply_text(_('GENERIC_UNKNOWN_ERROR'))
                logging.warn(e)

    def _remove_task(self, bot, update, task):
        task.delete()

    def _start_remove_all_tasks(self, bot, update):
        update.message.reply_text(_('ADMIN_UPDATER_TASKS_REMOVE_ALL_HELP'))
        return TasksHandlerState.REMOVING_ALL_TASKS

    def _remove_all_tasks(self, bot, update):
        Task.drop_collection()
        return self._start(bot, update)


class TeamsHandlerState(Enum):
    INITIAL = auto()
    ADDING_TEAM = auto()
    REMOVING_ALL_TEAMS = auto()


class TeamsHandler(ConversationHandler):
    def __init__(self, base_help):
        super(TeamsHandler, self).__init__(
            entry_points=[CommandHandler('teams', self._start)],
            states={
                TeamsHandlerState.INITIAL: [
                    CommandHandler('add', self._start_add_team),
                    CommandHandler('get', OperationOnObject(Team,
                        self._start, self._get_team).execute, pass_args=True),
                    CommandHandler('getall', OperationOnCollection(Team,
                        self._start, self._get_all_teams).execute),
                    CommandHandler('remove', OperationOnObject(Team,
                        self._start, self._remove_team).execute,
                        pass_args=True),
                    CommandHandler('removeall', self._start_remove_all_teams),
                    CommandHandler('cancel', self._cancel),
                    MessageHandler(Filters.all, self._start),
                ],
                TeamsHandlerState.ADDING_TEAM: [
                    CommandHandler('cancel', self._start),
                    MessageHandler(Filters.all, self._add_team),
                ],
                TeamsHandlerState.REMOVING_ALL_TEAMS: [
                    CommandHandler('confirm', self._remove_all_teams),
                    MessageHandler(Filters.all, self._start),
                ],
            },
            fallbacks=[MessageHandler(Filters.all, self._fallback)]
        )
        self.base_help = base_help

    def _start(self, bot, update):
        update.message.reply_text(_('ADMIN_UPDATER_TEAMS_HELP'))
        return TeamsHandlerState.INITIAL

    def _cancel(self, bot, update):
        self.base_help(bot, update)
        return ConversationHandler.END

    def _fallback(self, bot, update):
        update.message.reply_text(_('GENERIC_UNKNOWN_ERROR'))
        logging.warn('Unknown command in the current state: %s' %
            update.message.text)
        return ConversationHandler.END

    def _start_add_team(self, bot, update):
        update.message.reply_text(_('ADMIN_UPDATER_TEAMS_ADD_HELP'))
        return TeamsHandlerState.ADDING_TEAM

    def _add_team(self, bot, update):
        msg = update.message

        game_stage = get_current_game_stage()
        if game_stage != GameStage.PRE_REGISTRATION:
            msg.reply_text(_('ADMIN_UPDATER_TEAMS_ADD_UNAVAILABLE'))
            return self._start(bot, update)

        try:
            name = (msg.text or "").strip()
            if name == "":
                msg.reply_text(_('ADMIN_UPDATER_TEAMS_ADD_INVALID_NAME'))
                return TeamsHandlerState.ADDING_TEAM

            Team(name=name).save()
            msg.reply_text(_('ADMIN_UPDATER_TEAMS_ADD_SUCCESS'))

            return self._start(bot, update)
        except Exception as e:
            msg.reply_text(_('GENERIC_UNKNOWN_ERROR'))
            logging.warn(e)
        return self._start_add_team(bot, update)

    def _get_team(self, bot, update, team):
        participants = Participant.objects(team_id=team.id)
        text = ('{team_name} ({team_id})\n' +
            'Участники ({ppl_count}): {participants}').format(
                team_name=team.name, team_id=team.id,
                ppl_count=participants.count(),
                participants=", ".join([ p.name for p in participants]))
        update.message.reply_text(text)

    def _get_all_teams(self, bot, update, teams):
        for team in teams:
            self._get_team(bot, update, team)

    def _remove_team(self, bot, update, team):
        team.delete()

    def _start_remove_all_teams(self, bot, update):
        update.message.reply_text(_('ADMIN_UPDATER_TEAMS_REMOVE_ALL_HELP'))
        return TeamsHandlerState.REMOVING_ALL_TEAMS

    def _remove_all_teams(self, bot, update):
        Team.drop_collection()
        return self._start(bot, update)


class GameStateHandlerState(Enum):
    INITIAL = auto()


class GameStateHandler(ConversationHandler):
    def __init__(self, base_help, main_updater):
        super(GameStateHandler, self).__init__(
            entry_points=[CommandHandler('game_state', self._start)],
            states={
                GameStateHandlerState.INITIAL: [
                    CommandHandler('open_registration', self._open_registration),
                    CommandHandler('change_to', self._change_to, pass_args=True),
                    CommandHandler('start_game', self._start_game),
                    CommandHandler('stop_game', self._stop_game),
                    CommandHandler('reset_game', self._reset_game),
                    CommandHandler('cancel', self._cancel),
                    MessageHandler(Filters.all, self._start),
                ],
            },
            fallbacks=[]
        )
        self.base_help = base_help
        self.main_updater = main_updater

    def _start(self, bot, update):
        game_stage = get_current_game_stage()
        update.message.reply_text(_('ADMIN_UPDATER_GAME_STATE_HELP').format(
            game_stage=game_stage.name))
        return GameStateHandlerState.INITIAL

    def _cancel(self, bot, update):
        self.base_help(bot, update)
        return ConversationHandler.END

    def _change_to(self, bot, update, args):
        new_stage_str = args[0] if len(args) > 0 else ""
        if not new_stage_str in GameStage.__members__:
            update.message.reply_text(_('ADMIN_UPDATER_GAME_STATE_INVALID'
                ).format(stage=new_stage_str))
        else:
            set_current_game_stage(GameStage[new_stage_str])
        return self._start(bot, update)

    def _open_registration(self, bot, update):
        return self._change_to(bot, update, [GameStage.REGISTRATION.name])

    def _start_game(self, bot, update):
        set_current_game_stage(GameStage.REGISTRATION_CLOSED)

        teams = list(Team.objects)
        random.shuffle(teams)
        tasks = list(Task.objects)
        random.shuffle(tasks)

        if len(tasks) < len(teams):
            update.message.reply_text(_('ADMIN_UPDATER_GAME_STATE_NOT_ENOUGH_TASKS'
                ).format(team_count=len(teams), task_count=len(tasks)))
            return self._start(bot, update)

        for i in range(len(teams)):
            teams[i].task_id = tasks[i].id
            teams[i].save()

        set_current_game_stage(GameStage.SUBMITTING)
        self.main_updater.notify_game_starts()

        return self._start(bot, update)

    def _stop_game(self, bot, update):
        return self._change_to(bot, update, [GameStage.REGISTRATION_CLOSED.name])

    def _reset_game(self, bot, update):
        return self._change_to(bot, update, [GameStage.PRE_REGISTRATION.name])
