#!/usr/bin/env pytho

import logging

from telegram import Document
from bson.objectid import ObjectId

from zmshgame.handlers.constants import GIF_MIME_TYPE
from zmshgame.l10n import get_message as _
from zmshgame.models import GameStage
from zmshgame.models import GameState
from zmshgame.models import Task

def task_to_document(task):
    return task.file_url


def get_current_game_stage():
    game_state = GameState.objects.first()
    return GameStage[game_state.current_stage]


def set_current_game_stage(new_stage):
    game_state = GameState.objects.first()
    game_state.current_stage = new_stage.name
    game_state.save()


class OperationOnObject:
    def __init__(self, object_type, fallback, operation):
        self.object_type = object_type
        self.fallback = fallback
        self.operation = operation

    def execute(self, bot, update, args):
        try:
            if len(args) < 1:
                update.message.reply_text(_('GENERIC_NO_ID'))
            else:
                want_id = ObjectId(args[0])
                matching_tasks = self.object_type.objects(id=want_id)
                if len(matching_tasks) < 1:
                    update.message.reply_text(_('GENERIC_NOT_FOUND'))
                else:
                    self.operation(bot, update, matching_tasks.first())
        except Exception as e:
            update.message.reply_text(_('GENERIC_UNKNOWN_ERROR'))
            logging.warn(e)
        return self.fallback(bot, update)


class OperationOnCollection(object):
    def __init__(self, object_type, fallback, operation):
        self.object_type = object_type
        self.fallback = fallback
        self.operation = operation

    def execute(self, bot, update):
        try:
            self.operation(bot, update, self.object_type.objects)
        except Exception as e:
            update.message.reply_text(_('GENERIC_UNKNOWN_ERROR'))
            logging.warn(e)
        return self.fallback(bot, update)
