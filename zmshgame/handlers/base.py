#!/usr/bin/env python

import logging

import telegram.ext as tgext

from zmshgame.l10n import get_message as _

class BaseUpdater(tgext.Updater):
    def __init__(self, *args):
        super(BaseUpdater, self).__init__(*args)
        self.dispatcher.add_error_handler(self._handle_error)

    def _handle_error(self, bot, update, error):
        logging.warning('Update "%s" caused error "%s"', update, error)
