#!/usr/bin/env python

from datetime import datetime
from enum import Enum
from enum import auto
import logging
import random

from telegram.ext import CommandHandler
from telegram.ext import ConversationHandler
from telegram.ext import MessageHandler
from telegram.ext import Filters

from zmshgame.handlers.base import BaseUpdater
from zmshgame.handlers.utils import get_current_game_stage
from zmshgame.handlers.utils import task_to_document
from zmshgame.l10n import get_message as _
from zmshgame.models import GameStage
from zmshgame.models import Submission
from zmshgame.models import Participant
from zmshgame.models import Team
from zmshgame.models import Task

class MainHandlerState(Enum):
    INITIAL = auto()
    SUBMITTING = auto()


class MainUpdater(BaseUpdater):
    def __init__(self, *args):
        super(MainUpdater, self).__init__(*args)
        self._register_handlers()

    def notify_game_starts(self):
        for participant in Participant.objects:
            chat_id = participant.tg_chat_id
            if not chat_id is None:
                text = _('MAIN_UPDATER_HELP_GAME_STARTED')
                try:
                    self.bot.send_message(chat_id=chat_id, text=text)
                except Exception as e:
                    logging.warning('Failed to notify user in chat {chat_id} ' +
                        'about game start: {error}'.format(
                            chat_id=chat_id, error=e))

    def _register_handlers(self):
        dp = self.dispatcher
        dp.add_handler(ConversationHandler(
            entry_points=[MessageHandler(Filters.all, self._start)],
            states={
                MainHandlerState.INITIAL: [
                    CommandHandler('register', self._register),
                    CommandHandler('submit', self._start_submit),
                ],
                MainHandlerState.SUBMITTING: [
                    CommandHandler('cancel', self._start),
                    MessageHandler(Filters.all, self._submit),
                ],
            },
            fallbacks=[]
        ))

    def _get_current_participant(self, update):
        try:
            participants = Participant.objects(tg_id=update.effective_user.id)
            if len(participants) > 0:
                return participants.first()
        except Exception as e:
            logging.warn('Failed to obtain current participant: ' +
                '{error}'.format(error=e))
        return None

    def _print_help(self, bot, update):
        update.message.reply_text(_('MAIN_UPDATER_HELP'))

    def _start(self, bot, update):
        current_stage = get_current_game_stage()

        if current_stage == GameStage.PRE_REGISTRATION:
            update.message.reply_text(_('MAIN_UPDATER_HELP_PRE_REGISTRATION'))
            return ConversationHandler.END

        participant = self._get_current_participant(update)
        team = self._get_team(participant)
        if current_stage == GameStage.REGISTRATION:
            if not participant is None:
                update.message.reply_text(_('MAIN_UPDATER_ALREADY_REGISTERED'
                    ).format(name=participant.name, team_name=team.name))
                return ConversationHandler.END

            update.message.reply_text(_('MAIN_UPDATER_HELP_REGISTRATION'))
            return MainHandlerState.INITIAL

        if current_stage == GameStage.SUBMITTING and not participant is None:
            update.message.reply_text(_('MAIN_UPDATER_HELP_SUBMITTNG').format(
                name=participant.name, team_name=team.name))
            task = Task.objects(id=team.task_id).first()
            update.message.reply_video(task_to_document(task))
            return MainHandlerState.INITIAL

        update.message.reply_text(_('MAIN_UPDATER_HELP_POST_REGISTRATION'))
        return ConversationHandler.END

    def _get_team(self, participant):
        if participant is None:
            return None
        return Team.objects(id=participant.team_id).first()

    def _register(self, bot, update):
        current_stage = get_current_game_stage()

        if current_stage != GameStage.REGISTRATION:
            update.message.reply_text(_('MAIN_UPDATER_REGISTRATION_UNAVAILABLE'))
            return self._start(bot, update)

        participant = self._get_current_participant(update)
        if not participant is None:
            return self._start(bot, update)

        try:
            user = update.effective_user
            name = ('%s %s' % (user.first_name, user.last_name or '')).strip()
            team = min(Team.objects, key=lambda t: len(Participant.objects(team_id=t.id)))
            Participant(
                tg_id=user.id,
                tg_chat_id=update.effective_chat.id,
                name=name,
                team_id=team.id
            ).save()
            return self._start(bot, update)
        except Exception as e:
            update.message.reply_text(_('GENERIC_UNKNOWN_ERROR'))
            logging.warn(e)

        return self._start(bot, update)

    def _start_submit(self, bot, update):
        current_stage = get_current_game_stage()

        if current_stage != GameStage.SUBMITTING:
            update.message.reply_text(_('MAIN_UPDATER_SUBMITTING_UNAVAILABLE'))
            return self._start(bot, update)

        participant = self._get_current_participant(update)
        if participant is None:
            return self._start(bot, update)

        update.message.reply_text(_('MAIN_UPDATER_SUBMITTING_HELP'))
        return MainHandlerState.SUBMITTING

    def _submit(self, bot, update):
        try:
            msg = update.message
            participant = self._get_current_participant(update)
            Submission(
                team_id=participant.team_id,
                file_id=msg.document.file_id,
                file_type=msg.document.file_id,
                submit_time=datetime.now().strftime('%Y,%m,%d,%H,%M,%S,%f'),
            ).save()
            msg.reply_text(_('MAIN_UPDATER_SUBMISSION_SUCCESSFUL'))
            return self._start(bot, update)
        except Exception as e:
            update.message.reply_text(_('GENERIC_UNKNOWN_ERROR'))
            logging.warn(e)

        return self._start(bot, update)
