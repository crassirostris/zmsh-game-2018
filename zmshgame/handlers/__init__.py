#!/usr/bin/env python

from zmshgame.handlers.main import MainUpdater
from zmshgame.handlers.admin import AdminUpdater

__all__ = ('MainUpdater', 'AdminUpdater')
