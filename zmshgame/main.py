#!/usr/bin/env python

import argparse
import logging

import mongoengine

from zmshgame.handlers import MainUpdater
from zmshgame.handlers import AdminUpdater
from zmshgame.models import GameState

def _ParseArgs():
    parser = argparse.ArgumentParser(description='Process some integers.')

    parser.add_argument('--api-token', type=str, required=True,
                        help='a Telegram API token for the main handler.')
    parser.add_argument('--admin-api-token', type=str, required=True,
                        help='a Telegram API token for the admin handler.')

    parser.add_argument('--admins', type=str, required=True,
                        help='a comma-separated list of admins.')

    parser.add_argument('--db-name', type=str, required=True,
                        help='a database name.')
    parser.add_argument('--db-host', type=str, default='localhost',
                        help='a database hostname.')

    parser.add_argument('--verbose', type=bool, default=False,
                        help='whether to write verbose logs.')

    return parser.parse_args()


def _SetupLogging(verbose):
    level = logging.DEBUG if verbose else logging.INFO
    logging.basicConfig(level=level,
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')


def _SetupDatabase(db_name, db_host):
    mongoengine.connect(db=db_name, host=db_host)


def _SetupGameState():
    gss = GameState.objects
    if len(gss) == 0:
        GameState().save()
    elif len(gss) > 1:
        logging.error('More than one game state found in DB')
        raise RuntimeError('More than one game state found in DB')


def main():
    args = _ParseArgs()
    _SetupLogging(args.verbose)
    _SetupDatabase(args.db_name, args.db_host)
    _SetupGameState()

    main_updater = MainUpdater(args.api_token)
    main_updater.start_polling()

    admins = args.admins.split(',')
    admin_updater = AdminUpdater(main_updater, admins, args.admin_api_token)
    admin_updater.start_polling()

    main_updater.idle()


if __name__ == '__main__':
    main()
